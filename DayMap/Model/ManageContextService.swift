//
//  ManageContextService.swift
//  DayMap
//
//  Created by MacBook on 7/29/18.
//  Copyright © 2018 MacBook. All rights reserved.
//

import CoreData
import UIKit

let appDelegate = UIApplication.shared.delegate as? AppDelegate


class ManageContextService:NSObject {
    
     
    
    static func save(place:String,reason:String,completion:(_ completion:Bool)->()){
        guard let manageContext = appDelegate?.persistentContainer.viewContext else {return}
        let map = MappedDay(context: manageContext)
        map.place = place
        map.reason = reason
        //trying to say data
        do {
            try manageContext.save()
            completion(true)
        } catch {
            print("could not save data!")
            completion(false)
        }
    }
    
    static func fetch(completion:@escaping([MappedDay])->()){
        var result:[MappedDay] = []
        guard let manageContext = appDelegate?.persistentContainer.viewContext else {return}
        let fetch = NSFetchRequest<MappedDay>(entityName: "MappedDay")
        
        do{
            result = try manageContext.fetch(fetch)
        }catch{
            print("could not fetch: \(error.localizedDescription)")
        }
        completion(result)
    }
    
    static func delete(at indexPath:IndexPath, days:[MappedDay]){
        guard let manageContext = appDelegate?.persistentContainer.viewContext else {return}
        manageContext.delete(days[indexPath.row])
        do{
            try manageContext.save()
        }catch{
            print("could not delete")
        }
    }
    
   
    
}
