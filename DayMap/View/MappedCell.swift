//
//  MappedCell.swift
//  DayMap
//
//  Created by MacBook on 7/29/18.
//  Copyright © 2018 MacBook. All rights reserved.
//

import UIKit

class MappedCell: UITableViewCell {

    @IBOutlet weak var placeLbl:UILabel!
    @IBOutlet weak var reasonLbl:UILabel!
    static let identifier = "mapCell"
    
    func configureCell(map:MappedDay){
        placeLbl.text = map.place
        reasonLbl.text = map.reason
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        placeLbl.text = ""
        reasonLbl.text = ""
    }
    
}
