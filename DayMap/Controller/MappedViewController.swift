//
//  ViewController.swift
//  DayMap
//
//  Created by MacBook on 7/29/18.
//  Copyright © 2018 MacBook. All rights reserved.
//

import UIKit

class MappedViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    var dayMap = [MappedDay]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ManageContextService.fetch { (result) in
            //MappeData.fetchResult = result
            self.dayMap = result
            self.tableView.reloadData()
        }
    }
    
   
    @IBAction func sortMyMap(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            dayMap.reverse()
            tableView.reloadData()
        case 1:
            dayMap.reverse()
            tableView.reloadData()
        default:
            print("hey what are you doing?")
        }
    }
    
    

}

extension MappedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dayMap.count
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MappedCell.identifier) as? MappedCell else {return UITableViewCell()}
        cell.configureCell(map: dayMap[indexPath.row])
        return cell
    }
    //delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        ManageContextService.delete(at: indexPath, days: dayMap)
        tableView.reloadData()
    }
    
}

