//
//  AddToMapVC.swift
//  DayMap
//
//  Created by MacBook on 7/29/18.
//  Copyright © 2018 MacBook. All rights reserved.
//

import UIKit

class AddToMapVC: UIViewController {
    
    @IBOutlet weak var placeTextField:UITextField!
    @IBOutlet weak var reasonTextView:UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        reasonTextView.delegate = self
      
        //
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(addToMap))
        
    }
    
    @objc func addToMap(){
        if placeTextField.text != "" && reasonTextView.text != "" && reasonTextView.text != "What will you do there?" {
            ManageContextService.save(place: placeTextField.text!, reason: reasonTextView.text!) { (completed) in
                if completed {
                    //dismiis view controller and show root view controlelr
                    navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
 
}

extension AddToMapVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = .black
    }
}
